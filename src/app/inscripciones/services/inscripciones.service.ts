import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {Crearinscripcionrequestmodel} from "../interfaces/crearinscripcionrequestmodel.interface";

@Injectable({
  providedIn: 'root'
})
export class InscripcionesService {

  private baseUrl: string = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
  }

  guardarInscripcion(inscripcion: Crearinscripcionrequestmodel): Observable<number> {
    return this.httpClient.post<number>(this.baseUrl + '/inscripciones/', inscripcion);
  }
}
